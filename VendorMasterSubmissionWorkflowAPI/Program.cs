using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System.IO;

namespace VendorMasterSubmissionWorkflowAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateFileLoggerUsingJSONFile();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static void CreateFileLoggerUsingJSONFile()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            //Log.Logger = new LoggerConfiguration()
            //    .ReadFrom.Configuration(configuration)
            //    .CreateLogger();

            Log.Logger = new LoggerConfiguration()
                .WriteTo.AzureAnalytics(workspaceId: "3528a151-73f9-4d9b-9ae6-0ba152f693c6",
                authenticationId: "7ixPk4mhSAFc8ot890UYebWIWkJXiI47//IJ8LpNEbnJ9tbmsa6y8Xxr7lcdYU1D5uG2b8mV3yDv8Cq9VRbU6g==")
            .CreateLogger();

        }
    }
}