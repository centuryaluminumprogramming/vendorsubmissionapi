﻿using System.Net.Mail;
using System.Text;
using VendorMasterSubmissionWorkflowAPI.Models;

namespace VendorMasterSubmissionWorkflowAPI.Utilities
{
    public static class EmailUtility
    {
        public static void SendEmail(EmailMessage emailMessage)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(emailMessage.EmailServer);

            mail.From = new MailAddress(emailMessage.From);

            foreach (string emailaddress in emailMessage.EmailAddresses)
            {
                mail.To.Add(emailaddress);
            }
            mail.Subject = emailMessage.Subject;

            mail.Body = emailMessage.Body;
            mail.BodyEncoding = Encoding.UTF8;
            mail.IsBodyHtml = true;

            System.Net.NetworkCredential basicAuthenticationInfo = new
                System.Net.NetworkCredential(emailMessage.EmailAccount, emailMessage.EmailPassword);

            SmtpServer.Credentials = basicAuthenticationInfo;

            SmtpServer.Send(mail);
        }
    }
}