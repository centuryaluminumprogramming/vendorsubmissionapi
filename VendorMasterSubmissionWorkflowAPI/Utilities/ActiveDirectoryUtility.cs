﻿using System.Collections.Generic;
using System.DirectoryServices;
using VendorMasterSubmissionWorkflowAPI.Models;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowAPI.Utilities
{
    public static class ActiveDirectoryUtility
    {
        public static IList<User> ADSearch(string firstName, string lastName)
        {
            IList<User> users = new List<User>();

            if (firstName == null)
                firstName = "*";
            else
                firstName = "*" + firstName + "*";
            if (lastName == null)
                lastName = "*";
            else
                lastName = "*" + lastName + "*";

            DirectoryEntry rootDSE = new DirectoryEntry("LDAP://RootDSE");
            var defaultNamingContext = rootDSE.Properties["defaultNamingContext"].Value;

            //--- Code to use the current address for the LDAP and query it for the user---
            DirectorySearcher dssearch = new DirectorySearcher("LDAP://" + defaultNamingContext);

            dssearch.Filter = "(&(sn=" + lastName + ")(givenName=" + firstName + ")(objectClass=user)(!userAccountControl:1.2.840.113556.1.4.803:=2))";

            SearchResultCollection daresult = dssearch.FindAll();

            foreach (SearchResult searchResult in daresult)
            {
                if (searchResult.Properties["samaccountname"][0] != null)
                {
                    User user = new User();
                    var FirstName = searchResult.Properties["givenName"][0].ToString();
                    var LastName = searchResult.Properties["sn"][0].ToString();

                    var Login = searchResult.Properties["samaccountname"][0].ToString();

                    user.Login = Login;
                    user.DisplayName = FirstName + " " + LastName;
                    user.EmailAddress = searchResult.Properties["mail"][0].ToString();
                    users.Add(user);
                }
            }
            return users;
        }

        public static string GetFullNameFromLogin(string login)
        {
            DirectoryEntry rootDSE = new DirectoryEntry("LDAP://RootDSE");
            var defaultNamingContext = rootDSE.Properties["defaultNamingContext"].Value;

            //--- Code to use the current address for the LDAP and query it for the user---
            DirectorySearcher dssearch = new DirectorySearcher("LDAP://" + defaultNamingContext);

            dssearch.Filter = "(samaccountname=" + login + ")";

            SearchResult daresult = dssearch.FindOne();

            var FirstName = daresult.Properties["givenName"][0].ToString();
            var LastName = daresult.Properties["sn"][0].ToString();
            var fullname = FirstName + " " + LastName;

            return fullname;
        }

        public static string GetEamilFromLogin(string login)
        {
            DirectoryEntry rootDSE = new DirectoryEntry("LDAP://RootDSE");
            var defaultNamingContext = rootDSE.Properties["defaultNamingContext"].Value;

            //--- Code to use the current address for the LDAP and query it for the user---
            DirectorySearcher dssearch = new DirectorySearcher("LDAP://" + defaultNamingContext);

            dssearch.Filter = "(samaccountname=" + login + ")";

            SearchResult daresult = dssearch.FindOne();

            var email = daresult.Properties["mail"][0].ToString();

            return email;
        }
    }
}