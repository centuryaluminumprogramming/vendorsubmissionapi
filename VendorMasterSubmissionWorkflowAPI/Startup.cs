using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using VendorMasterSubmissionWorkflowAPI.Extensions;
using VendorMasterSubmissionWorkflowAPI.Extentions;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Repository;
using VendorMasterSubmissionWorkflowAPI.Workflow;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Hosting;

namespace VendorMasterSubmissionWorkflowAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public class Constants
        {
            public const string CORS_ORIGINS = "CorsOrigins";
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IApprovalRequestRepository, ApprovalRequestRespository>();
            services.AddScoped<IEmployeeVendorRequestRepository, EmployeeVendorRequestRepository>();
            services.AddScoped<IGroupRepository, GroupRepository>();
            services.AddScoped<IManufacturingVendorRequestRepository, ManufacturingVendorRequestRepository>();
            services.AddScoped<INonManufacturingVendorRequestRepository, NonManufacturingVendorRequestRepository>();
            services.AddScoped<IVendorChangeRequestRepository, VendorChangeRequestRepository>();
            services.AddScoped<IVendorDocumentRepository, VendorDocumentRepository>();
            services.AddScoped<INotification, Notification>();

            services.AddControllers();
            services.AddAuthentication(IISDefaults.AuthenticationScheme);

            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsPolicy", builder => builder
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins(Configuration.GetSection("CorsOrigins").Get<string[]>())
                    .AllowCredentials());
            });
            //services.ConfigureCors();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "VendorMasterSubmissionWorkflowAPI", Version = "v1" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "VendorMasterSubmissionWorkflowAPI v1"));
                app.UseCors(options =>
                {
                    options.WithOrigins(Configuration.GetSection("CorsOrigins").Get<string[]>());
                    options.AllowAnyMethod();
                    options.AllowAnyHeader();
                    options.AllowCredentials();
                });
            }

            app.ConfigureExceptionHandler();

            app.UseRouting();
            app.UseCors("CorsPolicy");


            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}