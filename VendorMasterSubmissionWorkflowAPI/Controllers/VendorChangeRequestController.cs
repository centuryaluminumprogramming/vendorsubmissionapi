﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowDAL.Repository;

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{

    //[Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class VendorChangeRequestController : ControllerBase

    {
        private readonly IConfiguration _config;
        private readonly IVendorChangeRequestRepository _vendorChangeRequestRepository;

        public VendorChangeRequestController(IConfiguration config, IVendorChangeRequestRepository vendorChangeRequestRepository)

        {
            _config = config;
            _vendorChangeRequestRepository = vendorChangeRequestRepository;
        }

        // GET: api/<EmployeeVendorRequestController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            IList<VendorChangeRequest> vendorChangeRequests = await _vendorChangeRequestRepository.GetAsync();

            if (vendorChangeRequests != null)
                return Ok(vendorChangeRequests);
            else
                return NotFound();
        }

        // GET api/<EmployeeVendorRequestController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(string id)
        {
            VendorChangeRequest vendorChangeRequest = await _vendorChangeRequestRepository.GetAsync(id);

            if (vendorChangeRequest != null)
                return Ok(vendorChangeRequest);
            else
                return NotFound();
        }

        // POST api/<EmployeeVendorRequestController>
        [HttpPost]
        public IActionResult Post(VendorChangeRequest vendorChangeRequest)
        {
            return Ok(_vendorChangeRequestRepository.Add(vendorChangeRequest));
        }

        // PUT api/<EmployeeVendorRequestController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(string id, VendorChangeRequest vendorChangeRequest)
        {
            await _vendorChangeRequestRepository.UpdateAsync(id, vendorChangeRequest);
            return NoContent();
        }

        // DELETE api/<EmployeeVendorRequestController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _vendorChangeRequestRepository.DeleteAsync(id);
            return NoContent();
        }

        // GET: api/<EmployeeVendorRequestController>
        [Route("search")]
        [HttpGet]
        public async Task<IActionResult> Get(string username, string stage, string sapVendorNumber)
        {
            IVendorChangeRequestRepository vendorChangeRequestRepository = new VendorChangeRequestRepository(_config);
            IList<VendorChangeRequest> vendorChangeRequests = await _vendorChangeRequestRepository.GetAsync(username, stage, sapVendorNumber);

            if (vendorChangeRequests != null)
                return Ok(vendorChangeRequests);
            else
                return NotFound();
        }


        [Route("comment")]
        [HttpPost]
        public IActionResult Post(string requestID, Comment comment)
        {

            _vendorChangeRequestRepository.AddCommentToRequest(requestID, comment);

            return NoContent();


        }

    }
}