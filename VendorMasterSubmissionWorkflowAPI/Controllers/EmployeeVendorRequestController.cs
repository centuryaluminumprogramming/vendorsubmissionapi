﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

using VendorMasterSubmissionWorkflowAPI.ViewModels;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeVendorRequestController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IEmployeeVendorRequestRepository _employeeVendorRequestRepository;

        public EmployeeVendorRequestController(IConfiguration config, IEmployeeVendorRequestRepository employeeVendorRequestRepository)

        {
            _config = config;
            _employeeVendorRequestRepository = employeeVendorRequestRepository;
        }

        // GET: api/<EmployeeVendorRequestController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            IList<EmployeeVendorRequest> employeeVendorRequests = await _employeeVendorRequestRepository.GetAsync();

            if (employeeVendorRequests != null)
                return Ok(employeeVendorRequests);
            else
                return NotFound();
        }

        // GET api/<EmployeeVendorRequestController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(string id)
        {
            EmployeeVendorRequest employeeVendorRequest = await _employeeVendorRequestRepository.GetAsync(id);

            if (employeeVendorRequest != null)
                return Ok(employeeVendorRequest);
            else
                return NotFound();
        }

        // POST api/<EmployeeVendorRequestController>
        [HttpPost]
        public IActionResult Post(EmployeeVendorRequest employeeVendorRequest)
        {
            RequestReturn requestReturn = new RequestReturn();
            requestReturn.ObjectId = _employeeVendorRequestRepository.Add(employeeVendorRequest);

            return Ok(requestReturn);
        }

        // PUT api/<EmployeeVendorRequestController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(string id, EmployeeVendorRequest employeeVendorRequest)
        {
            await _employeeVendorRequestRepository.UpdateAsync(id, employeeVendorRequest);
            return NoContent();
        }

        // DELETE api/<EmployeeVendorRequestController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _employeeVendorRequestRepository.DeleteAsync(id);
            return NoContent();
        }

        // GET: api/<EmployeeVendorRequestController>
        [Route("search")]
        [HttpGet]
        public async Task<IActionResult> Get(string username, string stage, string sapVendorNumber)
        {
            IList<EmployeeVendorRequest> employeeVendorRequests = await _employeeVendorRequestRepository.GetAsync(username, stage, sapVendorNumber);

            if (employeeVendorRequests != null)
                return Ok(employeeVendorRequests);
            else
                return NotFound();
        }


        [Route("comment")]
        [HttpPost]
        public IActionResult Post(string requestID, Comment comment)
        {

            _employeeVendorRequestRepository.AddCommentToRequest(requestID, comment);

            return NoContent();


        }
    }
}