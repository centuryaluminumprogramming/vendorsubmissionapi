﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{

    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class NonManufacturingVendorRequestController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly INonManufacturingVendorRequestRepository _nonManufacturingVendorRequestRepository;

        public NonManufacturingVendorRequestController(IConfiguration config, INonManufacturingVendorRequestRepository nonManufacturingVendorRequestRepository)

        {
            _config = config;
            _nonManufacturingVendorRequestRepository = nonManufacturingVendorRequestRepository;
        }

        // GET: api/<EmployeeVendorRequestController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            IList<NonManufacturingVendorRequest> nonManufacturingVendorRequests = await _nonManufacturingVendorRequestRepository.GetAsync();

            if (nonManufacturingVendorRequests != null)
                return Ok(nonManufacturingVendorRequests);
            else
                return NotFound();
        }

        // GET api/<EmployeeVendorRequestController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(string id)
        {
            NonManufacturingVendorRequest nonManufacturingVendorRequest = await _nonManufacturingVendorRequestRepository.GetAsync(id);
            if (nonManufacturingVendorRequest != null)
                return Ok(nonManufacturingVendorRequest);
            else
                return NotFound();
        }

        // POST api/<EmployeeVendorRequestController>
        [HttpPost]
        public IActionResult Post(NonManufacturingVendorRequest nonManufacturingVendorRequest)
        {
            return Ok(_nonManufacturingVendorRequestRepository.Add(nonManufacturingVendorRequest));
        }

        // PUT api/<EmployeeVendorRequestController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(string id, NonManufacturingVendorRequest nonManufacturingVendorRequest)
        {
            await _nonManufacturingVendorRequestRepository.UpdateAsync(id, nonManufacturingVendorRequest);
            return NoContent();
        }

        // DELETE api/<EmployeeVendorRequestController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _nonManufacturingVendorRequestRepository.DeleteAsync(id);
            return NoContent();
        }

        // GET: api/<EmployeeVendorRequestController>
        [Route("search")]
        [HttpGet]
        public async Task<IActionResult> Get(string username, string stage, string sapVendorNumber)
        {
            IList<NonManufacturingVendorRequest> nonManufacturingVendorRequests = await _nonManufacturingVendorRequestRepository.GetAsync(username, stage, sapVendorNumber);

            if (nonManufacturingVendorRequests != null)
                return Ok(nonManufacturingVendorRequests);
            else
                return NotFound();
        }


        [Route("comment")]
        [HttpPost]
        public IActionResult Post(string requestID, Comment comment)
        {

            _nonManufacturingVendorRequestRepository.AddCommentToRequest(requestID, comment);

            return NoContent();


        }

    }
}