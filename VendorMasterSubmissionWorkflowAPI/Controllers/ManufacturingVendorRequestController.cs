﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{

    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ManufacturingVendorRequestController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IManufacturingVendorRequestRepository _manufacturingVendorRequestRepository;

        public ManufacturingVendorRequestController(IConfiguration config, IManufacturingVendorRequestRepository manufacturingVendorRequestRepository)

        {
            _config = config;
            _manufacturingVendorRequestRepository = manufacturingVendorRequestRepository;
        }

        // GET: api/<EmployeeVendorRequestController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            IList<ManufacturingVendorRequest> manufacturingVendorRequests = await _manufacturingVendorRequestRepository.GetAsync();

            if (manufacturingVendorRequests != null)
                return Ok(manufacturingVendorRequests);
            else
                return NotFound();
        }

        // GET api/<EmployeeVendorRequestController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(string id)
        {
            ManufacturingVendorRequest manufacturingVendorRequest = await _manufacturingVendorRequestRepository.GetAsync(id);
            if (manufacturingVendorRequest != null)
                return Ok(manufacturingVendorRequest);
            else
                return NotFound();
        }

        // POST api/<EmployeeVendorRequestController>
        [HttpPost]
        public IActionResult Post(ManufacturingVendorRequest manufacturingVendorRequest)
        {
            return Ok(_manufacturingVendorRequestRepository.Add(manufacturingVendorRequest));
        }

        // PUT api/<EmployeeVendorRequestController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(string id, ManufacturingVendorRequest manufacturingVendorRequest)
        {
            await _manufacturingVendorRequestRepository.UpdateAsync(id, manufacturingVendorRequest);
            return NoContent();
        }

        // DELETE api/<EmployeeVendorRequestController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _manufacturingVendorRequestRepository.DeleteAsync(id);
            return NoContent();
        }

        // GET: api/<EmployeeVendorRequestController>
        [Route("search")]
        [HttpGet]
        public async Task<IActionResult> Get(string username, string stage, string sapVendorNumber)
        {
            IList<ManufacturingVendorRequest> manufacturingVendorRequests = await _manufacturingVendorRequestRepository.GetAsync(username, stage, sapVendorNumber);

            if (manufacturingVendorRequests != null)
                return Ok(manufacturingVendorRequests);
            else
                return NotFound();
        }

        [Route("comment")]
        [HttpPost]
        public IActionResult Post(string requestID, Comment comment)
        {

            _manufacturingVendorRequestRepository.AddCommentToRequest(requestID, comment);

            return NoContent();


        }
    }

}
