﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowAPI.Workflow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]

    public class ApprovalRequestController : ControllerBase

    {
        private readonly IConfiguration _config;
        private readonly IApprovalRequestRepository _approvalRequestRepository;
        private readonly INotification _notification;

        public ApprovalRequestController(IConfiguration config, IApprovalRequestRepository approvalRequestRepository, INotification notification)

        {
            _config = config;
            _approvalRequestRepository = approvalRequestRepository;
            _notification = notification;
            
        }

        // GET: api/<ApprovalController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            //Task<IList<ApprovalRequest>>

            IList<ApprovalRequest> approvalRequests = await _approvalRequestRepository.GetAsync();
            if (approvalRequests != null)
                return Ok(approvalRequests);
            else
                return NotFound();
        }

        // GET api/<ApprovalController>/5
        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(string Id)
        {
            Task<ApprovalRequest> approvalRequest = _approvalRequestRepository.GetAsync(Id);

            if (approvalRequest != null)
                return Ok(approvalRequest);
            else
                return NotFound();
        }

        // POST api/<ApprovalController>
        [HttpPost]
        public IActionResult Post([FromBody] ApprovalRequest approvalRequest)
        {
            _approvalRequestRepository.Add(approvalRequest);
            _notification.SendApprovalNotificationAsync(approvalRequest);
            return NoContent();
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _approvalRequestRepository.DeleteAsync(id);
            return NoContent();
        }


        [Route("Group")]
        [HttpGet]
        public async Task<IActionResult> GetApprovalsForTeamAsync(string group)
        {
            IList<ApprovalRequest> approvalRequests = await _approvalRequestRepository.GetByGroupAsync(group);

            if (approvalRequests != null)
                return Ok(approvalRequests);
            else
                return NotFound();
        }

        [Route("VendorRequest")]
        [HttpGet]
        public async Task<IActionResult> GetApprovalsForVendorRequestAsync(string id)
        {
            IList<ApprovalRequest> approvalRequests = await _approvalRequestRepository.GetByRequestAsync(id);

            if (approvalRequests != null)
                return Ok(approvalRequests);
            else
                return NotFound();
        }
    }
}