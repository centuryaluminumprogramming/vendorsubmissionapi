﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IGroupRepository _groupRepository;

        public UserController(IConfiguration config, IGroupRepository groupRepository)

        {
            _config = config;
            _groupRepository = groupRepository;
        }

        // GET: api/<UserController>

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IList<User> users = await _groupRepository.GetAllUsersAsync();

            return Ok(users);
        }

        // GET api/<UserController>/5

        [Route("search")]
        [HttpGet]
        public IActionResult Get(string firstname, string lastname)
        {
            var users = VendorMasterSubmissionWorkflowAPI.Utilities.ActiveDirectoryUtility.ADSearch(firstname, lastname);
            return Ok(users);
        }

        // GET api/<UserController>/5
        [HttpGet("{login}")]
        public IActionResult Get(string login)
        {
            User user = new User();
            string userFullName = VendorMasterSubmissionWorkflowAPI.Utilities.ActiveDirectoryUtility.GetFullNameFromLogin(login);

            if (userFullName != null)
            {
                user.DisplayName = userFullName;
                user.Login = login;
                user.EmailAddress = VendorMasterSubmissionWorkflowAPI.Utilities.ActiveDirectoryUtility.GetEamilFromLogin(login);
                List<string> GroupInfo = new List<string>();

                var myGroups = _groupRepository.GetUsersGroups(login);
                foreach (var item in myGroups.Result)
                {
                    GroupInfo.Add(item.Name + " - " + item.Id);
                    user.Group = GroupInfo;
                }               
        
                return Ok(user);
            }
            else
                return NotFound();
        }

        [Route("Group")]
        [HttpPut]
        public IActionResult Put(string userID, string groupID)
        {
            string userFullName = VendorMasterSubmissionWorkflowAPI.Utilities.ActiveDirectoryUtility.GetFullNameFromLogin(userID);

            if (userFullName != null)
            {
                User user = new User();
                user.DisplayName = userFullName;
                user.Login = userID;
                user.EmailAddress = VendorMasterSubmissionWorkflowAPI.Utilities.ActiveDirectoryUtility.GetEamilFromLogin(userID);
                _groupRepository.AddUserToGroup(groupID, user);
            }

            return NoContent();
        }

        [Route("Group")]
        [HttpDelete]
        public IActionResult Delete(string userID, string groupID)
        {
            _groupRepository.DeleteUserFromGroup(groupID, userID);
            return NoContent();
        }

        [Route("Group")]
        [HttpGet]
        public IActionResult GetUserGroups(string login)
        {
            var groups = _groupRepository.GetUsersGroups(login);

            if (groups != null)
                return Ok(groups);
            else
                return
                    NotFound();
        }

        [Route("Current")]
        [HttpGet]
        public IActionResult GetCurrentUser()
        {
            string username = HttpContext.User?.Identity?.Name ?? "N/A";

            return Ok(username);
        }

    }
}