﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IGroupRepository _groupRepository;

        public GroupController(IConfiguration config, IGroupRepository groupRepository)

        {
            _config = config;
            _groupRepository = groupRepository;
        }

        // GET: api/<GroupController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            IList<Group> groups = await _groupRepository.GetAsync();

            if (groups != null)
                return Ok(groups);
            else
                return NotFound();
        }

        // GET api/<GroupController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(string id)
        {
            Group group = await _groupRepository.GetAsync(id);

            if (group != null)
                return Ok(group);
            else
                return NotFound();
        }

        // POST api/<GroupController>
        [HttpPost]
        public IActionResult Post([FromBody] Group group)
        {
            _groupRepository.Add(group);
            return NoContent();
        }

        // PUT api/<GroupController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(string id, [FromBody] Group group)
        {
            await _groupRepository.UpdateAsync(id, group);
            return NoContent();
        }

        // DELETE api/<GroupController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _groupRepository.DeleteAsync(id);
            return NoContent();
        }
    }
}