﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowDAL.ViewModels;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IVendorDocumentRepository _vendorDocumentRepository;

        public DocumentController(IConfiguration config, IVendorDocumentRepository vendorDocumentRepository)

        {
            _config = config;
            _vendorDocumentRepository = vendorDocumentRepository;
        }

        // GET api/<DocumentController>/5
        [HttpGet("{id}")]
        public async Task<FileStreamResult> GetAsync(string id)
        {
            VendorDocument vendorDocument = await _vendorDocumentRepository.GetAsync(id);

            var dataStreem = new MemoryStream(vendorDocument.File);
            return File(dataStreem, "application/octet-stream", vendorDocument.FileName);
        }

        // POST api/<DocumentController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromForm] DocumentViewModel documentViewModel)
        {
            VendorDocument vendorDocument = new VendorDocument();

            vendorDocument.FileName = documentViewModel.SubmittedDocument.FileName;

            var stream = new MemoryStream((int)documentViewModel.SubmittedDocument.Length);
            documentViewModel.SubmittedDocument.CopyTo(stream);
            var bytes = stream.ToArray();

            vendorDocument.VendorRequestID = documentViewModel.VendorRequestID;
            vendorDocument.VendorRequestType = documentViewModel.VendorRequestType;
            vendorDocument.DocumentType = documentViewModel.DocumentType;
            vendorDocument.File = bytes;
            _vendorDocumentRepository.Add(vendorDocument);

            return NoContent();
        }



        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _vendorDocumentRepository.DeleteAsync(id);
            return NoContent();
        }


        [Route("VendorRequest")]
        [HttpGet]
        public async Task<IEnumerable<DocumentListViewModel>> GetVendorRequestDocumentsAsync(string vendorRequestId)
        {
            IList<DocumentListViewModel> vendorDocumentList = await _vendorDocumentRepository.GetVendorRequestDocumentListAsync(vendorRequestId);
            return vendorDocumentList;
        }
    }
}