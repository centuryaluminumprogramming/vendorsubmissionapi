﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VendorMasterSubmissionWorkflowAPI.Workflow;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ApprovalResponseController : ControllerBase
    {
        private readonly IConfiguration _config;

        public ApprovalResponseController(IConfiguration config)

        {
            _config = config;
        }

        // POST api/<ApprovalResponseController>
        [HttpPost]
        public IActionResult Post([FromBody] ApprovalResponse approvalResponse)
        {
            ApprovalWorkflow approvalWorkflow = new ApprovalWorkflow(_config);
            approvalWorkflow.AdvanceWorkflowAsync(approvalResponse);
            return NoContent();
        }
    }
}