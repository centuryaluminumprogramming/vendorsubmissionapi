﻿using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowAPI.Workflow
{
    public interface INotification
    {
        public Task SendApprovalNotificationAsync(ApprovalRequest approvalRequest);

    }
}
