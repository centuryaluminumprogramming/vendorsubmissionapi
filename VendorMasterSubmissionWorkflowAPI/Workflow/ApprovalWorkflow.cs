﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowDAL.Repository;

namespace VendorMasterSubmissionWorkflowAPI.Workflow
{
    public class ApprovalWorkflow
    {
        private readonly IConfiguration _config;

        public ApprovalWorkflow(IConfiguration config)

        {
            _config = config;
        }

        public async Task AdvanceWorkflowAsync(ApprovalResponse workflowChange)
        {
            IApprovalRequestRepository approvalRequestRespository = new ApprovalRequestRespository(_config);
            ApprovalRequest currentApproval = await approvalRequestRespository.GetAsync(workflowChange.ApprovalID);

            switch (currentApproval.VendorRequestType)
            {
                case "EmployeeVendor":
                    await AdvanceEmployeeAsync(workflowChange, currentApproval);
                    break;

                case "ManufacturingVendor":
                    await AdvanceManufacturing(workflowChange, currentApproval);
                    break;

                case "NonManufacturingVendor":
                    await AdvanceNonManufacturing(workflowChange, currentApproval);
                    break;

                case "VendorChange":
                    await AdvanceChangeAsync(workflowChange, currentApproval);
                    break;

                default:
                    throw new InvalidOperationException("Undefined Vendor Request Type");
            }
        }

        private async Task AdvanceEmployeeAsync(ApprovalResponse workflowChange, ApprovalRequest approvalRequest)

        {


            INotification notification = new Notification(_config);

            IEmployeeVendorRequestRepository employeeVendorRequestRepository = new EmployeeVendorRequestRepository(_config);
            EmployeeVendorRequest employeeVendorRequest = await employeeVendorRequestRepository.GetAsync(approvalRequest.VendorRequestID);
            approvalRequest.ApprovalStatus = workflowChange.ApprovalStatus;
            approvalRequest.ApproveDenyTime = DateTime.Now;
            approvalRequest.Approver = workflowChange.UserMakingChange;
            IApprovalRequestRepository approvalRequestRespository = new ApprovalRequestRespository(_config);
            await approvalRequestRespository.UpdateAsync(approvalRequest.Id, approvalRequest);

            if (workflowChange.ApprovalStatus == "Rejected")
            {
                employeeVendorRequest.Stage = "Rejected " + approvalRequest.ApprovalStage;
            }
            else
            {
                switch (approvalRequest.ApprovalStage)
                {
                    case "Human Resources Review":
                        {
                            employeeVendorRequest.Stage = "Shared Services Review";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "Shared Services";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Shared Services Review";
                            approvalRequestRespository.Add(approval);

                            await notification.SendApprovalNotificationAsync(approvalRequest);

                            break;
                        }

                    case "Shared Services Review":
                        {
                            employeeVendorRequest.Stage = "Treasury Verification";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "Treasury";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Treasury Verification";
                            approvalRequestRespository.Add(approval);

                            await notification.SendApprovalNotificationAsync(approvalRequest);

                            break;
                        }
                    case "Treasury Verification":
                        {
                            employeeVendorRequest.Stage = "Treasury 15 Day Hold";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "System";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Treasury 15 Day Hold";
                            approvalRequestRespository.Add(approval);



                            break;
                        }
                    case "Treasury 15 Day Hold":
                        employeeVendorRequest.Stage = "Complete";
                        break;

                    default:
                        throw new InvalidOperationException("Undefined Workflow Stage");
                }
            }

            employeeVendorRequest.LastApprover = workflowChange.UserMakingChange;
            employeeVendorRequest.StatusChangeDate = DateTime.Now;
            await employeeVendorRequestRepository.UpdateAsync(employeeVendorRequest.Id, employeeVendorRequest);
        }

        private async Task AdvanceManufacturing(ApprovalResponse workflowChange, ApprovalRequest approvalRequest)

        {
            INotification notification = new Notification(_config);

            IManufacturingVendorRequestRepository manufacturingVendorRequestRepository = new ManufacturingVendorRequestRepository(_config);
            ManufacturingVendorRequest manufacturingVendorRequest = await manufacturingVendorRequestRepository.GetAsync(approvalRequest.VendorRequestID);
            IApprovalRequestRepository approvalRequestRespository = new ApprovalRequestRespository(_config);
            approvalRequest.ApprovalStatus = workflowChange.ApprovalStatus;
            approvalRequest.ApproveDenyTime = DateTime.Now;
            approvalRequest.Approver = workflowChange.UserMakingChange;
            await approvalRequestRespository.UpdateAsync(approvalRequest.Id, approvalRequest);

            if (workflowChange.ApprovalStatus == "Rejected")
            {
                manufacturingVendorRequest.Stage = "Rejected " + approvalRequest.ApprovalStage;
            }
            else
            {
                switch (approvalRequest.ApprovalStage)
                {
                    case "Buyer Review":
                        {
                            manufacturingVendorRequest.Stage = "Shared Services Review";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "Shared Services";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Shared Services Review";
                            approvalRequestRespository.Add(approval);

                            await notification.SendApprovalNotificationAsync(approvalRequest);

                            break;
                        }

                    case "Shared Services Review":
                        {
                            manufacturingVendorRequest.Stage = "Treasury Verification";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "Treasury";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Treasury Verification";
                            approvalRequestRespository.Add(approval);

                            await notification.SendApprovalNotificationAsync(approvalRequest);

                            break;
                        }
                    case "Treasury Verification":
                        {
                            if (manufacturingVendorRequest.VendorRequest.FifteenDayWaiver == false)
                            {
                                manufacturingVendorRequest.Stage = "Treasury 15 Day Hold";
                                ApprovalRequest approval = new ApprovalRequest();
                                approval.ApprovalCreationTime = DateTime.Now;
                                approval.ApprovalGroup = "System";
                                approval.VendorRequestID = approvalRequest.VendorRequestID;
                                approval.VendorRequestType = approvalRequest.VendorRequestType;
                                approval.ApprovalStage = "Treasury 15 Day Hold";
                                approvalRequestRespository.Add(approval);
                            }
                            else
                            {
                                manufacturingVendorRequest.Stage = "Complete";
                            }
                            break;
                        }
                    case "Treasury 15 Day Hold":
                        manufacturingVendorRequest.Stage = "Complete";
                        break;

                    default:
                        throw new InvalidOperationException("Undefined Workflow Stage");
                }
            }

            manufacturingVendorRequest.LastApprover = workflowChange.UserMakingChange;
            manufacturingVendorRequest.StatusChangeDate = DateTime.Now;
            await manufacturingVendorRequestRepository.UpdateAsync(manufacturingVendorRequest.Id, manufacturingVendorRequest);
        }

        private async Task AdvanceNonManufacturing(ApprovalResponse workflowChange, ApprovalRequest approvalRequest)
        {

            INotification notification = new Notification(_config);

            INonManufacturingVendorRequestRepository nonManufacturingVendorRequestRepository = new NonManufacturingVendorRequestRepository(_config);
            NonManufacturingVendorRequest nonManufacturingVendorRequest = await nonManufacturingVendorRequestRepository.GetAsync(approvalRequest.VendorRequestID);
            ApprovalRequestRespository approvalRequestRespository = new ApprovalRequestRespository(_config);
            approvalRequest.ApprovalStatus = workflowChange.ApprovalStatus;
            approvalRequest.ApproveDenyTime = DateTime.Now;
            approvalRequest.Approver = workflowChange.UserMakingChange;
            await approvalRequestRespository.UpdateAsync(approvalRequest.Id, approvalRequest);

            if (workflowChange.ApprovalStatus == "Rejected")
            {
                nonManufacturingVendorRequest.Stage = "Rejected " + approvalRequest.ApprovalStage;
            }
            else
            {
                switch (approvalRequest.ApprovalStage)
                {
                    case "Shared Services Review":
                        {
                            nonManufacturingVendorRequest.Stage = "Treasury Verification";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "Treasury";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Treasury Verification";
                            approvalRequestRespository.Add(approval);

                            await notification.SendApprovalNotificationAsync(approvalRequest);

                            break;
                        }
                    case "Treasury Verification":
                        {
                            if (nonManufacturingVendorRequest.VendorRequest.FifteenDayWaiver == false)
                            {

                                nonManufacturingVendorRequest.Stage = "Treasury 15 Day Hold";
                                ApprovalRequest approval = new ApprovalRequest();
                                approval.ApprovalCreationTime = DateTime.Now;
                                approval.ApprovalGroup = "System";
                                approval.VendorRequestID = approvalRequest.VendorRequestID;
                                approval.VendorRequestType = approvalRequest.VendorRequestType;
                                approval.ApprovalStage = "Treasury 15 Day Hold";
                                approvalRequestRespository.Add(approval);
                            }
                            else
                            {
                                nonManufacturingVendorRequest.Stage = "Complete";
                            }
                            break;
                        }
                    case "Treasury 15 Day Hold":
                        nonManufacturingVendorRequest.Stage = "Complete";
                        break;

                    default:
                        throw new InvalidOperationException("Undefined Workflow Stage");
                }
            }

            nonManufacturingVendorRequest.LastApprover = workflowChange.UserMakingChange;
            nonManufacturingVendorRequest.StatusChangeDate = DateTime.Now;
            await nonManufacturingVendorRequestRepository.UpdateAsync(nonManufacturingVendorRequest.Id, nonManufacturingVendorRequest);
        }

        private async Task AdvanceChangeAsync(ApprovalResponse workflowChange, ApprovalRequest approvalRequest)
        {
            INotification notification = new Notification(_config);

            IVendorChangeRequestRepository vendorChangeRequestRepository = new VendorChangeRequestRepository(_config);
            VendorChangeRequest vendorChangeRequest = await vendorChangeRequestRepository.GetAsync(approvalRequest.VendorRequestID);
            ApprovalRequestRespository approvalRequestRespository = new ApprovalRequestRespository(_config);
            approvalRequest.ApprovalStatus = workflowChange.ApprovalStatus;
            approvalRequest.ApproveDenyTime = DateTime.Now;
            approvalRequest.Approver = workflowChange.UserMakingChange;
            await approvalRequestRespository.UpdateAsync(approvalRequest.Id, approvalRequest);

            if (workflowChange.ApprovalStatus == "Rejected")
            {
                vendorChangeRequest.Stage = "Rejected " + approvalRequest.ApprovalStage;
            }
            else
            {
                switch (approvalRequest.ApprovalStage)
                {
                    case "Shared Services Review":
                        {
                            vendorChangeRequest.Stage = "Treasury Verification";
                            ApprovalRequest approval = new ApprovalRequest();
                            approval.ApprovalCreationTime = DateTime.Now;
                            approval.ApprovalGroup = "Treasury";
                            approval.VendorRequestID = approvalRequest.VendorRequestID;
                            approval.VendorRequestType = approvalRequest.VendorRequestType;
                            approval.ApprovalStage = "Treasury Verification";
                            approvalRequestRespository.Add(approval);

                            await notification.SendApprovalNotificationAsync(approvalRequest);

                            break;
                        }
                    case "Treasury Verification":
                        {
                            if (vendorChangeRequest.VendorRequest.FifteenDayWaiver == false)
                            {
                                vendorChangeRequest.Stage = "Treasury 15 Day Hold";
                                ApprovalRequest approval = new ApprovalRequest();
                                approval.ApprovalCreationTime = DateTime.Now;
                                approval.ApprovalGroup = "System";
                                approval.VendorRequestID = approvalRequest.VendorRequestID;
                                approval.VendorRequestType = approvalRequest.VendorRequestType;
                                approval.ApprovalStage = "Treasury 15 Day Hold";
                                approvalRequestRespository.Add(approval);

                            }
                            else
                                vendorChangeRequest.Stage = "Complete";
                            break;
                        }
                    case "Treasury 15 Day Hold":
                        vendorChangeRequest.Stage = "Complete";
                        break;

                    default:
                        throw new InvalidOperationException("Undefined Workflow Stage");
                }
            }

            vendorChangeRequest.LastApprover = workflowChange.UserMakingChange;
            vendorChangeRequest.StatusChangeDate = DateTime.Now;
            await vendorChangeRequestRepository.UpdateAsync(vendorChangeRequest.Id, vendorChangeRequest);
        }
    }
}