﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowAPI.Models;
using VendorMasterSubmissionWorkflowAPI.Utilities;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowDAL.Repository;

namespace VendorMasterSubmissionWorkflowAPI.Workflow
{
    public class Notification : INotification
    {
        private IConfiguration _configuration;
        private string _emailServer;
        private string _senderEmail;
        private string _emailAccount;
        private string _emailPassword;
        private string _uiURL;
        private string _emailAddressOverride;

        public Notification(IConfiguration configuration)
        {
            _configuration = configuration;
            _emailServer = _configuration["EmailServer"];
            _senderEmail = _configuration["SenderEmail"];
            _emailAccount = _configuration["emailAccount"];
            _emailPassword = _configuration["emailPassword"];
            _emailAddressOverride = _configuration["EmailAddressOverride"];
            _uiURL = _configuration["UIURL"];
        }

        private string GetNotificationURLPath(string vendorRequestType)
        {
            switch (vendorRequestType)
            {
                case "EmployeeVendor":
                    return "employee-interviewee-request-details";

                case "ManufacturingVendor":

                    return "manufacturing-request-details";

                case "NonManufacturingVendor":

                    return "non-manufacturing-request-details";

                case "VendorChange":

                    return "vendor-change-request-details";
                default:
                    return null;
            }
        }
        public async Task SendApprovalNotificationAsync(ApprovalRequest approvalRequest )
        {
            EmailMessage emailMessage = new EmailMessage();
            IGroupRepository groupRepository = new GroupRepository(_configuration);

            emailMessage.EmailAddresses = new List<string>();

            Group group = await groupRepository.GetByNameAsync(approvalRequest.ApprovalGroup);

            IList<User> users = group.Members;

            if (_emailAddressOverride != null)
                emailMessage.EmailAddresses.Add(_emailAddressOverride);
            else
            {
                if (users != null)
                {

                    foreach (User user in users)
                    {
                        emailMessage.EmailAddresses.Add(user.EmailAddress);
                    }
                }
            }
            emailMessage.EmailServer = _emailServer;
            emailMessage.EmailAccount = _emailAccount;
            emailMessage.EmailPassword = _emailPassword;
            emailMessage.From = _senderEmail;
            emailMessage.Subject = "Vendor Submission Approval Required";
            emailMessage.Body = "Please log on to " + _uiURL + " to approve a new vendor set-up";
            EmailUtility.SendEmail(emailMessage);

        }
    }
}