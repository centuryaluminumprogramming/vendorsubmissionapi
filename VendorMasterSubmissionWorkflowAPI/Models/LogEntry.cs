﻿namespace VendorMasterSubmissionWorkflowAPI.Models
{
    public class LogEntry
    {
        public string Application { get; set; }
        public string Section { get; set; }
        public string User { get; set; }
        public string LogType { get; set; }
        public string Detail { get; set; }
    }
}