﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendorMasterSubmissionWorkflowAPI.Models
{
    public class EmailMessage
    {
        public IList<string> EmailAddresses { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
        public string EmailServer { get; set; }
        public string EmailAccount { get; set; }
        public string EmailPassword { get; set; }

    }
}
