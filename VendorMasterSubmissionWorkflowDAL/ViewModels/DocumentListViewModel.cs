﻿namespace VendorMasterSubmissionWorkflowDAL.ViewModels
{
    public class DocumentListViewModel
    {
        public string DocumentID { get; set; }
        public string DocumentType { get; set; }
    }
}