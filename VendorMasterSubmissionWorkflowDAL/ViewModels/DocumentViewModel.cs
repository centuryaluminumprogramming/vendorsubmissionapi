﻿using Microsoft.AspNetCore.Http;

namespace VendorMasterSubmissionWorkflowDAL.ViewModels
{
    public class DocumentViewModel
    {
        public string VendorRequestType { get; set; }
        public string VendorRequestID { get; set; }
        public string DocumentType { get; set; }
        public IFormFile SubmittedDocument { get; set; }
    }
}