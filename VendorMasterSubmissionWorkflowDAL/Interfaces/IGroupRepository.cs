﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;


namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface IGroupRepository
    {
        public void Add(Group group);

        public Task<IList<Group>> GetAsync();

        public Task<Group> GetAsync(string Id);

        public Task UpdateAsync(string Id, Group group);

        public Task DeleteAsync(string Id);

        public Task AddUserToGroup(string groupID, User user);

        public Task DeleteUserFromGroup(string groupID, string userLogin);

        public Task<IList<Group>> GetUsersGroups(string userLogin);

        public Task<IList<User>> GetAllUsersAsync();

        public Task<Group> GetByNameAsync(string name);
    }
}