﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;


namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface IVendorChangeRequestRepository
    {
        public string Add(VendorChangeRequest vendorChangeRequest);

        public Task<IList<VendorChangeRequest>> GetAsync();

        public Task<VendorChangeRequest> GetAsync(string Id);

        public Task UpdateAsync(string Id, VendorChangeRequest vendorChangeRequest);

        public Task DeleteAsync(string Id);

        public Task<IList<VendorChangeRequest>> GetAsync(string username, string stage, string sapVendorNumber);

        public Task AddCommentToRequest(string ID, Comment comment);
    }
}