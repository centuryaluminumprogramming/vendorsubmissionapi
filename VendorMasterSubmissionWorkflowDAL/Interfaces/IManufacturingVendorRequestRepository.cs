﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;


namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface IManufacturingVendorRequestRepository
    {
        public string Add(ManufacturingVendorRequest manufacturingVendorRequest);

        public Task<IList<ManufacturingVendorRequest>> GetAsync();

        public Task<ManufacturingVendorRequest> GetAsync(string Id);

        public Task UpdateAsync(string Id, ManufacturingVendorRequest manufacturingVendorRequest);

        public Task DeleteAsync(string Id);

        public Task<IList<ManufacturingVendorRequest>> GetAsync(string username, string stage, string sapVendorNumber);

        public Task AddCommentToRequest(string ID, Comment comment);
    }
}