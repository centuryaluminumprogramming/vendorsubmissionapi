﻿using System.Collections.Generic;
using System.Threading.Tasks;

using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface IApprovalRequestRepository
    {
        public void Add(ApprovalRequest approvalRequest);

        public Task<IList<ApprovalRequest>> GetAsync();

        public Task<ApprovalRequest> GetAsync(string Id);

        public Task UpdateAsync(string Id, ApprovalRequest approvalRequest);

        public Task DeleteAsync(string Id);

        public Task<IList<ApprovalRequest>> GetByGroupAsync(string group);

        public Task<IList<ApprovalRequest>> GetByRequestAsync(string vendorRequestId);
    }
}