﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;


namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface IEmployeeVendorRequestRepository
    {
        public string Add(EmployeeVendorRequest employeeVendorRequest);

        public Task<IList<EmployeeVendorRequest>> GetAsync();

        public Task<EmployeeVendorRequest> GetAsync(string Id);

        public Task UpdateAsync(string Id, EmployeeVendorRequest employeeVendorRequest);

        public Task DeleteAsync(string Id);

        public Task<IList<EmployeeVendorRequest>> GetAsync(string username, string stage, string sapVendorNumber);

        public Task AddCommentToRequest(string ID, Comment comment);
    }
}