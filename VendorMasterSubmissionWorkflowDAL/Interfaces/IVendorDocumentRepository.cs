﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowDAL.ViewModels;

namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface IVendorDocumentRepository
    {
        public string Add(VendorDocument vendorDocument);

        public Task<VendorDocument> GetAsync(string Id);

        public Task<IList<DocumentListViewModel>> GetVendorRequestDocumentListAsync(string vendorRequestID);

        public Task DeleteAsync(string Id);
    }
}