﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Models;


namespace VendorMasterSubmissionWorkflowDAL.Interfaces
{
    public interface INonManufacturingVendorRequestRepository
    {
        public string Add(NonManufacturingVendorRequest nonManufacturingVendorRequest);

        public Task<IList<NonManufacturingVendorRequest>> GetAsync();

        public Task<NonManufacturingVendorRequest> GetAsync(string Id);

        public Task UpdateAsync(string Id, NonManufacturingVendorRequest nonManufacturingVendorRequest);

        public Task DeleteAsync(string Id);

        public Task<IList<NonManufacturingVendorRequest>> GetAsync(string username, string stage, string sapVendorNumber);

        public Task AddCommentToRequest(string ID, Comment comment);
    }
}