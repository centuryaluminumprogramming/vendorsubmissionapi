﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class VendorDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string VendorRequestType { get; set; }
        public string VendorRequestID { get; set; }
        public string DocumentType { get; set; }
        public byte[] File { get; set; }
        public string FileName { get; set; }
    }
}