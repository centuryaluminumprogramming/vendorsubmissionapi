﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class User
    {
        public string Login { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }

        public List<string> Group { get; set; }
    }
}