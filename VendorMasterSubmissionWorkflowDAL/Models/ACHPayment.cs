﻿namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class ACHPayment
    {
        public bool PhoneNumberChecked { get; set; }
        public bool PhoneNumberMatch { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool EmailConfirmed { get; set; }
        public string DifferentEmail { get; set; }
        public string BankName { get; set; }
        public string Account { get; set; }
    }
}