﻿namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class ApprovalResponse
    {
        public string ApprovalID { get; set; }
        public string UserMakingChange { get; set; }
        public string ApprovalStatus { get; set; }
    }
}