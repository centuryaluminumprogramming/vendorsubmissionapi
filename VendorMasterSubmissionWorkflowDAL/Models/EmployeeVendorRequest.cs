﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public enum EmployeeType
    {
        Employee,
        Interview
    }

    public class EmployeeVendorRequest
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public EmployeeType Type { get; set; }
        public string WillBeHired { get; set; }
        public string SAPVendorNumber { get; set; }
        public DateTime SubmissionDate { get; set; }
        public DateTime ReceiptDate { get; set; }

        public string BankName { get; set; }
        public string Account { get; set; }
        //        public IList<VendorDocument> Attachments { get; set; }

        public string Stage { get; set; }

        public string Submitter { get; set; }

        public DateTime StatusChangeDate { get; set; }
        public string LastApprover { get; set; }

        public IList<Comment> Comments { get; set; }
    }
}