﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class Comment
    {
        public string Type { get; set; }
        public string Detail { get; set; }
        public string Submitter { get; set; }

    }
}
