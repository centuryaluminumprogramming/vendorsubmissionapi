﻿using System;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public enum Payment
    {
        Check,
        ACH
    }

    public class VendorRequest
    {
        public string SAPVendorNumber { get; set; }
        public DateTime ReceiptDate { get; set; }
        public Payment PaymentType { get; set; }
        public CheckPayment CheckPayment { get; set; }
        public ACHPayment ACHPayment { get; set; }
        public bool FifteenDayWaiver { get; set; }
        public DateTime RequestReceiptDate { get; set; }
        // public IList<VendorDocument> Attachements { get; set; }
    }
}