﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class VendorChangeRequest
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public VendorRequest VendorRequest { get; set; }
        public string Stage { get; set; }

        public string Submitter { get; set; }

        public DateTime StatusChangeDate { get; set; }

        public DateTime SubmissionDate { get; set; }

        public string LastApprover { get; set; }

        public IList<Comment> Comments { get; set; }
    }
}