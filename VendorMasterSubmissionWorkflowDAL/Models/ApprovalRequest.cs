﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace VendorMasterSubmissionWorkflowDAL.Models
{
    public class ApprovalRequest
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string VendorRequestType { get; set; }
        public string VendorRequestID { get; set; }
        public string ApprovalStage { get; set; }
        public string ApprovalGroup { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalCreationTime { get; set; }
        public DateTime ApproveDenyTime { get; set; }
        public string Approver { get; set; }
    }
}