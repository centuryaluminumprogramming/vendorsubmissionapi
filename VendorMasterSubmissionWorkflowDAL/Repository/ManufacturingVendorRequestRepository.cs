﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Repository
{
    public class ManufacturingVendorRequestRepository : IManufacturingVendorRequestRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public ManufacturingVendorRequestRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public string Add(ManufacturingVendorRequest manufacturingVendorRequest)
        {
            if (manufacturingVendorRequest.Comments == null)
                manufacturingVendorRequest.Comments = new List<Comment>();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");

            manufacturingVendorRequest.SubmissionDate = DateTime.Now;

            dbCollection.InsertOne(manufacturingVendorRequest);
            return manufacturingVendorRequest.Id;
        }

        public async Task<IList<ManufacturingVendorRequest>> GetAsync()
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");

            FilterDefinitionBuilder<ManufacturingVendorRequest> filter = Builders<ManufacturingVendorRequest>.Filter;
            FilterDefinition<ManufacturingVendorRequest> emptyFilter = filter.Empty;

            IAsyncCursor<ManufacturingVendorRequest> allDocuments = await dbCollection.FindAsync<ManufacturingVendorRequest>(emptyFilter).ConfigureAwait(false);

            return allDocuments.ToList();
        }

        public async Task<ManufacturingVendorRequest> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;

            ManufacturingVendorRequest manufacturingVendorRequest = new ManufacturingVendorRequest();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");

            manufacturingVendorRequest = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return manufacturingVendorRequest;
        }

        public async Task UpdateAsync(string Id, ManufacturingVendorRequest manufacturingVendorRequest)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");
            manufacturingVendorRequest.Id = Id;
            var replaceOneResult = await dbCollection.ReplaceOneAsync(
            doc => doc.Id == Id, manufacturingVendorRequest);
            return;
        }

        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }

        public async Task<IList<ManufacturingVendorRequest>> GetAsync(string username, string stage, string sapVendorNumber)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");
            var filter = Builders<ManufacturingVendorRequest>.Filter.Where(s => s.Submitter == username || s.Stage == stage || s.VendorRequest.SAPVendorNumber == sapVendorNumber);

            var manufacturingVendorRequests = (IList<ManufacturingVendorRequest>)dbCollection.Find(filter).ToList();
            return manufacturingVendorRequests;
        }

        public async Task AddCommentToRequest(string ID, Comment comment)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ManufacturingVendorRequest> dbCollection = db.GetCollection<ManufacturingVendorRequest>("ManufacturingVendorRequests");

            var filter = Builders<ManufacturingVendorRequest>.Filter.Where(s => s.Id == ID);

            var update = Builders<ManufacturingVendorRequest>.Update
             .AddToSet(x => x.Comments, comment);
            var updateResult = await dbCollection.UpdateOneAsync(filter, update);

            return;
        }
    }
}