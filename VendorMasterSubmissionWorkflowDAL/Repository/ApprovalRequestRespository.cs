﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Repository

{
    public class ApprovalRequestRespository : IApprovalRequestRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public ApprovalRequestRespository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public void Add(ApprovalRequest approvalRequest)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");

            dbCollection.InsertOne(approvalRequest);
        }

        public async Task<IList<ApprovalRequest>> GetAsync()
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");

            FilterDefinitionBuilder<ApprovalRequest> filter = Builders<ApprovalRequest>.Filter;
            FilterDefinition<ApprovalRequest> emptyFilter = filter.Empty;

            IAsyncCursor<ApprovalRequest> allDocuments = await dbCollection.FindAsync<ApprovalRequest>(emptyFilter).ConfigureAwait(false);
            return allDocuments.ToList();
        }

        public async Task<ApprovalRequest> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;
            ApprovalRequest approvalRequest = new ApprovalRequest();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");
            approvalRequest = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return approvalRequest;
        }

        public async Task UpdateAsync(string Id, ApprovalRequest approvalRequest)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");
            approvalRequest.Id = Id;
            var replaceOneResult = await dbCollection.ReplaceOneAsync(
            doc => doc.Id == Id, approvalRequest);
            return;
        }

        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }

        public async Task<IList<ApprovalRequest>> GetByGroupAsync(string group)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");
            var filter = Builders<ApprovalRequest>.Filter.Where(s => s.ApprovalGroup == group);

            var approvalRequests = (IList<ApprovalRequest>)dbCollection.Find(filter).ToList();

            return approvalRequests;
        }

        public async Task<IList<ApprovalRequest>> GetByRequestAsync(string vendorRequestId)
        {
            if (!ObjectId.TryParse(vendorRequestId, out _))
                return null;

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<ApprovalRequest> dbCollection = db.GetCollection<ApprovalRequest>("ApprovalRequest");
            var filter = Builders<ApprovalRequest>.Filter.Where(s => s.VendorRequestID == vendorRequestId);

            var approvalRequests = (IList<ApprovalRequest>)dbCollection.Find(filter).ToList();

            return approvalRequests;
        }
    }
}