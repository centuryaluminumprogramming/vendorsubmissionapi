﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Repository
{
    public class EmployeeVendorRequestRepository : IEmployeeVendorRequestRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public EmployeeVendorRequestRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public string Add(EmployeeVendorRequest employeeVendorRequest)
        {
            if (employeeVendorRequest.Comments == null)
                employeeVendorRequest.Comments = new List<Comment>();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");

            employeeVendorRequest.SubmissionDate = DateTime.Now;

            dbCollection.InsertOne(employeeVendorRequest);

            return employeeVendorRequest.Id;
        }

        public async Task<IList<EmployeeVendorRequest>> GetAsync()
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");

            FilterDefinitionBuilder<EmployeeVendorRequest> filter = Builders<EmployeeVendorRequest>.Filter;
            FilterDefinition<EmployeeVendorRequest> emptyFilter = filter.Empty;

            IAsyncCursor<EmployeeVendorRequest> allDocuments = await dbCollection.FindAsync<EmployeeVendorRequest>(emptyFilter).ConfigureAwait(false);

            return allDocuments.ToList();
        }

        public async Task<EmployeeVendorRequest> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;

            EmployeeVendorRequest employeeVendorRequest = new EmployeeVendorRequest();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");
            employeeVendorRequest = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return employeeVendorRequest;
        }

        public async Task UpdateAsync(string Id, EmployeeVendorRequest employeeVendorRequest)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");
            employeeVendorRequest.Id = Id;
            var replaceOneResult = await dbCollection.ReplaceOneAsync(
            doc => doc.Id == Id, employeeVendorRequest);

            return;
        }

        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }

        public async Task<IList<EmployeeVendorRequest>> GetAsync(string username, string stage, string sapVendorNumber)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");
            var filter = Builders<EmployeeVendorRequest>.Filter.Where(s => s.Submitter == username || s.Stage == stage || s.SAPVendorNumber == sapVendorNumber);

            var employeeVendorRequests = (IList<EmployeeVendorRequest>)dbCollection.Find(filter).ToList();
            return employeeVendorRequests;
        }

        public async Task AddCommentToRequest(string ID, Comment comment)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<EmployeeVendorRequest> dbCollection = db.GetCollection<EmployeeVendorRequest>("employeeVendorRequests");

            var filter = Builders<EmployeeVendorRequest>.Filter.Where(s => s.Id == ID);

            var update = Builders<EmployeeVendorRequest>.Update
             .AddToSet(x => x.Comments, comment);
            var updateResult = await dbCollection.UpdateOneAsync(filter, update);

            return;
        }
    }
}