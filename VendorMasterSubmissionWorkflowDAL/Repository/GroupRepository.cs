﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Repository
{
    public class GroupRepository : IGroupRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public GroupRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public void Add(Group group)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            if (group.Members == null)
                group.Members = new List<User>();
            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");

            dbCollection.InsertOne(group);
        }

        public async Task<IList<Group>> GetAsync()
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");

            FilterDefinitionBuilder<Group> filter = Builders<Group>.Filter;
            FilterDefinition<Group> emptyFilter = filter.Empty;

            IAsyncCursor<Group> allDocuments = await dbCollection.FindAsync<Group>(emptyFilter).ConfigureAwait(false);

            return allDocuments.ToList();
        }

        public async Task<Group> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;

            Group group = new Group();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");
            group = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return group;
        }

        public async Task UpdateAsync(string Id, Group group)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");
            group.Id = Id;
            var replaceOneResult = await dbCollection.ReplaceOneAsync(
            doc => doc.Id == Id, group);

            return;
        }

        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }

        public async Task AddUserToGroup(string groupID, User user)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");

            var filter = Builders<Group>.Filter.Where(s => s.Id == groupID);

            var update = Builders<Group>.Update
             .AddToSet(x => x.Members, user);
            var updateResult = await dbCollection.UpdateOneAsync(filter, update);

            return;
        }

        public async Task DeleteUserFromGroup(string groupID, string userLogin)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");

            var filter = Builders<Group>.Filter.Where(ym => ym.Id == groupID);
            var update = Builders<Group>.Update.PullFilter(ym => ym.Members, Builders<User>.Filter.Where(nm => nm.Login == userLogin));
            dbCollection.UpdateOne(filter, update);
            return;
        }

        public async Task<IList<Group>> GetUsersGroups(string userLogin)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");

            var groups = dbCollection.Find(f => f.Members.Any(fb => fb.Login == userLogin));
            IList<Group> groupList = groups.ToList();
            return groupList;
        }

        public async Task<IList<User>> GetAllUsersAsync()
        {
            IList<User> users = new List<User>();
            IList<Group> Groups = await GetAsync();
            foreach (Group group in Groups)
            {
                if (group.Members != null)
                {
                    foreach (User user in group.Members)
                    {
                        users.Add(user);
                    }
                }
            }

            IList<User> distinctUsers = users.GroupBy(x => x.Login).Select(y => y.First()).ToList();

            return distinctUsers;
        }

        public async Task<Group> GetByNameAsync(string name)
        {
            Group group = new Group();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<Group> dbCollection = db.GetCollection<Group>("Group");
            group = dbCollection.Find(document => document.Name == name).FirstOrDefault();
            return group;
        }
    }
}