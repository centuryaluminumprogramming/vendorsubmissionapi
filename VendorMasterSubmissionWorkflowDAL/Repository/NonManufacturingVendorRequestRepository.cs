﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Repository
{
    public class NonManufacturingVendorRequestRepository : INonManufacturingVendorRequestRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public NonManufacturingVendorRequestRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public string Add(NonManufacturingVendorRequest nonManufacturingVendorRequest)
        {
            if (nonManufacturingVendorRequest.Comments == null)
                nonManufacturingVendorRequest.Comments = new List<Comment>();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");

            nonManufacturingVendorRequest.SubmissionDate = DateTime.Now;

            dbCollection.InsertOne(nonManufacturingVendorRequest);
            return nonManufacturingVendorRequest.Id;
        }

        public async Task<IList<NonManufacturingVendorRequest>> GetAsync()
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");

            FilterDefinitionBuilder<NonManufacturingVendorRequest> filter = Builders<NonManufacturingVendorRequest>.Filter;
            FilterDefinition<NonManufacturingVendorRequest> emptyFilter = filter.Empty;

            IAsyncCursor<NonManufacturingVendorRequest> allDocuments = await dbCollection.FindAsync<NonManufacturingVendorRequest>(emptyFilter).ConfigureAwait(false);

            return allDocuments.ToList();
        }

        public async Task<NonManufacturingVendorRequest> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;

            NonManufacturingVendorRequest nonManufacturingVendorRequest = new NonManufacturingVendorRequest();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");
            nonManufacturingVendorRequest = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return nonManufacturingVendorRequest;
        }

        public async Task UpdateAsync(string Id, NonManufacturingVendorRequest nonManufacturingVendorRequest)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");
            nonManufacturingVendorRequest.Id = Id;
            var replaceOneResult = await dbCollection.ReplaceOneAsync(
            doc => doc.Id == Id, nonManufacturingVendorRequest);
            return;
        }

        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }

        public async Task<IList<NonManufacturingVendorRequest>> GetAsync(string username, string stage, string sapVendorNumber)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");
            var filter = Builders<NonManufacturingVendorRequest>.Filter.Where(s => s.Submitter == username || s.Stage == stage || s.VendorRequest.SAPVendorNumber == sapVendorNumber);

            var nonManufacturingVendorRequests = (IList<NonManufacturingVendorRequest>)dbCollection.Find(filter).ToList();
            return nonManufacturingVendorRequests;
        }

        public async Task AddCommentToRequest(string ID, Comment comment)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("NonManufacturingVendorRequest");

            var filter = Builders<NonManufacturingVendorRequest>.Filter.Where(s => s.Id == ID);

            var update = Builders<NonManufacturingVendorRequest>.Update
             .AddToSet(x => x.Comments, comment);
            var updateResult = await dbCollection.UpdateOneAsync(filter, update);

            return;
        }
    }
}