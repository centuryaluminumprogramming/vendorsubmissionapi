﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;
using VendorMasterSubmissionWorkflowDAL.ViewModels;

namespace VendorMasterSubmissionWorkflowDAL.Repository
{
    public class VendorDocumentRepository : IVendorDocumentRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public VendorDocumentRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public string Add(VendorDocument vendorDocument)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<VendorDocument> dbCollection = db.GetCollection<VendorDocument>("VendorDocument");

            dbCollection.InsertOne(vendorDocument);
            return vendorDocument.Id;
        }

        public async Task<VendorDocument> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;
            VendorDocument vendorDocument = new VendorDocument();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorDocument> dbCollection = db.GetCollection<VendorDocument>("VendorDocument");
            vendorDocument = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return vendorDocument;
        }

        public async Task<IList<DocumentListViewModel>> GetVendorRequestDocumentListAsync(string vendorRequestID)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorDocument> dbCollection = db.GetCollection<VendorDocument>("VendorDocument");
            var filter = Builders<VendorDocument>.Filter.Where(s => s.VendorRequestID == vendorRequestID);

            var vendorDocuments = (IList<VendorDocument>)dbCollection.Find(filter).ToList();

            IList<DocumentListViewModel> docList = new List<DocumentListViewModel>();

            foreach (VendorDocument document in vendorDocuments)
            {
                DocumentListViewModel documentListViewModel = new DocumentListViewModel();
                documentListViewModel.DocumentType = document.DocumentType;
                documentListViewModel.DocumentID = document.Id;

                docList.Add(documentListViewModel);
            }
            return docList;
        }




        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorDocument> dbCollection = db.GetCollection<VendorDocument>("VendorDocument");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }


    }
}