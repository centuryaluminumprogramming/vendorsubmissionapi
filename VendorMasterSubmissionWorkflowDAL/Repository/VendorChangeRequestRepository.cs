﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMasterSubmissionWorkflowDAL.Interfaces;
using VendorMasterSubmissionWorkflowDAL.Models;

namespace VendorMasterSubmissionWorkflowDAL.Repository
{
    public class VendorChangeRequestRepository : IVendorChangeRequestRepository
    {
        private IConfiguration _configuration;
        private string _mongodbConnection;
        private string _databaseName;

        public VendorChangeRequestRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _mongodbConnection = _configuration["MongoDBConnection"];
            _databaseName = _configuration["MongoDBDatabase"];
        }

        public string Add(VendorChangeRequest vendorChangeRequest)
        {
            if (vendorChangeRequest.Comments == null)
                vendorChangeRequest.Comments = new List<Comment>();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);
            IMongoCollection<VendorChangeRequest> dbCollection = db.GetCollection<VendorChangeRequest>("VendorChangeRequest");

            vendorChangeRequest.SubmissionDate = DateTime.Now;

            dbCollection.InsertOne(vendorChangeRequest);
            return vendorChangeRequest.Id;
        }

        public async Task<IList<VendorChangeRequest>> GetAsync()
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorChangeRequest> dbCollection = db.GetCollection<VendorChangeRequest>("VendorChangeRequest");

            FilterDefinitionBuilder<VendorChangeRequest> filter = Builders<VendorChangeRequest>.Filter;
            FilterDefinition<VendorChangeRequest> emptyFilter = filter.Empty;

            IAsyncCursor<VendorChangeRequest> allDocuments = await dbCollection.FindAsync<VendorChangeRequest>(emptyFilter).ConfigureAwait(false);

            return allDocuments.ToList();
        }

        public async Task<VendorChangeRequest> GetAsync(string Id)
        {
            if (!ObjectId.TryParse(Id, out _))
                return null;
            VendorChangeRequest vendorChangeRequest = new VendorChangeRequest();

            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorChangeRequest> dbCollection = db.GetCollection<VendorChangeRequest>("VendorChangeRequest");
            vendorChangeRequest = dbCollection.Find(document => document.Id == Id).FirstOrDefault();
            return vendorChangeRequest;
        }

        public async Task UpdateAsync(string Id, VendorChangeRequest vendorChangeRequest)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorChangeRequest> dbCollection = db.GetCollection<VendorChangeRequest>("VendorChangeRequest");
            vendorChangeRequest.Id = Id;
            var replaceOneResult = await dbCollection.ReplaceOneAsync(
            doc => doc.Id == Id, vendorChangeRequest);
            return;
        }

        public async Task DeleteAsync(string Id)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<NonManufacturingVendorRequest> dbCollection = db.GetCollection<NonManufacturingVendorRequest>("VendorChangeRequest");

            var deleteOneResult = await dbCollection.DeleteOneAsync(
            doc => doc.Id == Id);
            return;
        }

        public async Task<IList<VendorChangeRequest>> GetAsync(string username, string stage, string sapVendorNumber)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorChangeRequest> dbCollection = db.GetCollection<VendorChangeRequest>("VendorChangeRequest");
            var filter = Builders<VendorChangeRequest>.Filter.Where(s => s.Submitter == username || s.Stage == stage || s.VendorRequest.SAPVendorNumber == sapVendorNumber);

            var vendorChangeRequests = (IList<VendorChangeRequest>)dbCollection.Find(filter).ToList();
            return vendorChangeRequests;
        }

        public async Task AddCommentToRequest(string ID, Comment comment)
        {
            MongoClient client = new MongoClient(_mongodbConnection);
            IMongoDatabase db = client.GetDatabase(_databaseName);

            IMongoCollection<VendorChangeRequest> dbCollection = db.GetCollection<VendorChangeRequest>("VendorChangeRequest");

            var filter = Builders<VendorChangeRequest>.Filter.Where(s => s.Id == ID);

            var update = Builders<VendorChangeRequest>.Update
             .AddToSet(x => x.Comments, comment);
            var updateResult = await dbCollection.UpdateOneAsync(filter, update);

            return;
        }
    }
}